const bcrypt = require("bcrypt");
const User = require("../models/User");
const auth = require("../auth")
const Products = require("../models/Products")

module.exports.registerUser = (req,res)=>{

    const hashedPw = bcrypt.hashSync(req.body.password,10);

    let newUser = new User ({

        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPw,
        mobileNo: req.body.mobileNo
    })

    newUser.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

module.exports.loginUser = (req,res)=>{
   User.findOne({email:req.body.email})
   .then(foundUser =>{
    if(foundUser === null){
        return res.send({message: "No User Found."})
    } else {
        const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

        if(isPasswordCorrect){
            return res.send({accessToken: auth.createAccessToken(foundUser)});
        } else {
            return res.send({message: "Incorrect Password"});
        }
    }
   })
}

module.exports.setAsAdmin = (req,res)=>{
    let update = {
        isAdmin: true
    }
    User.findByIdAndUpdate(req.params.userId,update,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.getUserDetails = (req,res)=>{

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.checkoutOrder = async (req,res)=>{

    if(req.user.isAdmin){
        return res.send({message: "Action Forbidden"})
    }

    let isUserUpdated = await User.findById(req.user.id).then(user => {
        
        let newOrders = {
            totalAmount: req.body.totalAmount,
            products: req.body.products
        }

        user.orders.push(newOrders)
        return user.save()
        .then(user => true)
        .catch(err => err.message)
    })

    if(isUserUpdated !== true){
        return res.send({message:isUserUpdated})
    }

    let prodFunc = req.body.products.forEach(function(prod){
        // console.log(prod.productId)
        Products.findById(prod.productId).then(products=>{
            let buyer = {
                userId: req.user.id,
                quantity: prod.quantity
            }
            // console.log(prod.productId)
            products.orders.push(buyer);
            return products.save().then(products => true).catch(err => err.message);
        })
    })

    if(isUserUpdated){
        return res.send({message:"Thank you for buying. Your order is on the way!"})
    }
}

module.exports.getAuthenticatedOrders = (req,res)=>{
    if(req.user.isAdmin){
        return res.send({message: "Action Forbidden"})
    } else {

        User.findById(req.user.id)
        .then(result=> res.send(result.orders))
        .catch(error => res.send(error))
    }
    
}

module.exports.getAllUserOrders = (req,res)=>{
    User.find({})
     .then(result=>{
        let allUserOrders = result.map(function(field){

           if(field.isAdmin === true){
            return
           } else if(field.orders.length===0){
            return
           } else {
            return field.orders
           }

        })
        let filteredOrders = allUserOrders.filter(display=>display!= null)
        res.send(filteredOrders)
     }).catch(error=>res.send(error))

}

module.exports.displayProducts = (req,res)=>{

    User.findById(req.user.id)
    .then(result=>{
        if(result.orders.length !== 0){
            let productList = result.orders.map(function(field){
                return field.products
            })
            res.send(productList)
        } else {
            res.send("This user has no orders.")
        }
        
    })
    .catch(error=>res.send(error))
}






