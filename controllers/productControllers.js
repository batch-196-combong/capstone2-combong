const bcrypt = require("bcrypt");
const Products = require("../models/Products");
const auth = require("../auth")

module.exports.getActiveProducts = (req,res)=>{
	Products.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.addProducts = (req,res)=>{
	let newProduct = new Products({
		name:req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	newProduct.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.getSingleProducts = (req,res) =>{
	Products.findById(req.params.productId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.updateProducts = (req,res)=>{
	let update = {
		name:req.body.name,
		description: req.body.description,
		price:req.body.price
	}

	Products.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.archiveProducts = (req,res) =>  {
	let update = {
		isActive: false
	}
	Products.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.getAllOrders = (req,res)=>{
	 Products.find({})
	 .then(result=>{
	 	let allOrders = result.map(function(field){

	 		if(field.orders.length  === 0){
	 			return
	 		} else {
	 		return field.orders
	 	}
	 	})
	 	let filteredOrders = allOrders.filter(display=>display!= null)
	 	res.send(filteredOrders)
	 }).catch(error=>res.send(error))

	
	}

module.exports.activateProducts = (req,res) =>  {
	let update = {
		isActive: true
	}
	Products.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}
