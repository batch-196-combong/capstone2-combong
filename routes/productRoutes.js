const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers")
const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// Retrieve All Active Products
router.get('/activeProducts',productControllers.getActiveProducts)

// Create Products (Admin Only)
router.post('/addProducts',verify,verifyAdmin,productControllers.addProducts)

// Retrieve a Single Product
router.get('/singleProduct/:productId',productControllers.getSingleProducts)

// Update Products
router.put('/updateProducts/:productId',verify,verifyAdmin,productControllers.updateProducts);

// Archive Products
router.delete('/archiveProducts/:productId',verify,verifyAdmin,productControllers.archiveProducts);

// Retrieve All Product Orders (Admin Only)
router.get('/getAllOrders',verify, verifyAdmin, productControllers.getAllOrders)

// Archive Products (Admin Only)
router.put('/activateProducts/:productId',verify,verifyAdmin, productControllers.activateProducts)

module.exports = router;