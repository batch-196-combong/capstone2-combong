const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers")
const auth = require("../auth");
const{verify,verifyAdmin} = auth;

// Register
router.post('/', userControllers.registerUser);

// Login
router.post('/login',userControllers.loginUser);

// Get User Details
router.get('/getUserDetails',verify, userControllers.getUserDetails)

// Set User as Admin
router.put('/setAsAdmin/:userId', verify,verifyAdmin, userControllers.setAsAdmin)

// CheckOut
router.post('/checkoutOrder',verify,userControllers.checkoutOrder);

// Retrieve Authenticated Users
router.get('/getLoginUserOrders', verify, userControllers.getAuthenticatedOrders)

// Retrieve All User Orders 
router.get('/getAllUserOrders',verify,verifyAdmin,userControllers.getAllUserOrders)

// Display Products per Order
router.get('/displayProducts',verify, userControllers.displayProducts)

module.exports = router;

