const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type:String,
		required:[true, "Name of Product is Required"]
	},
	description: {
		type:String,
		required:[true, "Description is Required"]
	},
	price: {
		type: Number,
		required: [true, "Price is Required"]
	},
	isActive: {
		type:Boolean,
		default:true
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	orders: [
		{
			userId:{
				type:String,
				required:[true,"User ID is Required"]
			},
			quantity:{
				type:Number,
				required:[true,"Quantity 2 is Required"]
			},
			purchasedOn:{
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Products",productSchema)