const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type:String,
		required:[true,"Firts Name is Required"]
	},
	lastName: {
		type:String,
		required:[true,"lastName is Required"]
	},
	email: {
		type:String,
		required:[true,"Email is Required"]
	},
	password: {
		type:String,
		required:[true,"Password is Required"]
	},
	mobileNo: {
		type:String,
		required:[true,"Mobile Number is Required"]
	},
	isAdmin: {
		type:Boolean,
		default: false
	},
	orders: [
		{
			totalAmount: {
				type: Number,
				required:[true,"Total Amount is Required"]
			},
			purchasedOn:{
				type: Date,
				default: new Date()
			},
			products: [
				{
					productId: {
						type: String,
						required:[true,"Product ID is Required"]
					},
					quantity: {
						type:Number,
						required:[true,"Quantity 1 is Required"]
					}
				}
			]
		}
	]
})
module.exports = mongoose.model("User",userSchema);
