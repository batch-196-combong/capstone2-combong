const express= require("express");
const mongoose = require("mongoose");
const app = express();
const port = 5000;

mongoose.connect("mongodb+srv://admin:admin123@cluster0.pnw34.mongodb.net/ecommerceAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error',console.error.bind(console, "MongoDB Connection Error"));
db.once('open',()=>console.log("Connected to MongoDB"));

app.use(express.json());

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

const productRoutes = require('./routes/productRoutes')
app.use('/products',productRoutes);

app.listen(port,() => console.log(`Express API running at port 5000`))